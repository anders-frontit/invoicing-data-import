﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ConsoleApp1
{
    public class InvoiceEntryHistory
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Initials { get; set; }
        public DateTimeOffset ChangedAt { get; set; }
        public InvoiceEntryType EntryType { get; set; }
        public int ValueBefore { get; set; }
        public int ValueNow { get; set; }

        public int InvoiceId { get; set; }
        public Invoice Invoice { get; set; }

        public int InvoiceEntryId { get; set; }
        public InvoiceEntry InvoiceEntry { get; set; }

    }

    public enum InvoiceEntryType {
        None = 0,
        Suggested = 1,
        Approved = 2
    }
}
