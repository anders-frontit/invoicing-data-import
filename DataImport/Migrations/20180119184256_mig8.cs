﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FrontIT.DataImport.Migrations
{
    public partial class mig8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApprovedTime",
                table: "HistoricInvoiceEntries");

            migrationBuilder.DropColumn(
                name: "SuggestedTime",
                table: "HistoricInvoiceEntries");

            migrationBuilder.AddColumn<int>(
                name: "EntryType",
                table: "HistoricInvoiceEntries",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ValueBefore",
                table: "HistoricInvoiceEntries",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ValueNow",
                table: "HistoricInvoiceEntries",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EntryType",
                table: "HistoricInvoiceEntries");

            migrationBuilder.DropColumn(
                name: "ValueBefore",
                table: "HistoricInvoiceEntries");

            migrationBuilder.DropColumn(
                name: "ValueNow",
                table: "HistoricInvoiceEntries");

            migrationBuilder.AddColumn<int>(
                name: "ApprovedTime",
                table: "HistoricInvoiceEntries",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SuggestedTime",
                table: "HistoricInvoiceEntries",
                nullable: true);
        }
    }
}
