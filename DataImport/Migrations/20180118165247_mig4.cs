﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FrontIT.DataImport.Migrations
{
    public partial class mig4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HistoricInvoiceEntries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApprovedTime = table.Column<int>(nullable: false),
                    ChangedAt = table.Column<DateTime>(nullable: false),
                    Initials = table.Column<string>(nullable: true),
                    SuggestedTime = table.Column<int>(nullable: false),
                    TimeEntryId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistoricInvoiceEntries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HistoricInvoiceEntries_TimeEntries_TimeEntryId",
                        column: x => x.TimeEntryId,
                        principalTable: "TimeEntries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HistoricInvoiceEntries_TimeEntryId",
                table: "HistoricInvoiceEntries",
                column: "TimeEntryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistoricInvoiceEntries");
        }
    }
}
