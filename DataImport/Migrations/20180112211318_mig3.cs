﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FrontIT.DataImport.Migrations
{
    public partial class mig3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Invoices",
                newName: "Number");

            migrationBuilder.RenameColumn(
                name: "approvalStatus",
                table: "InvoiceEntries",
                newName: "ApprovalStatus");

            migrationBuilder.AddColumn<int>(
                name: "InvoiceId",
                table: "TimeEntries",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TimeEntries_InvoiceId",
                table: "TimeEntries",
                column: "InvoiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_TimeEntries_Invoices_InvoiceId",
                table: "TimeEntries",
                column: "InvoiceId",
                principalTable: "Invoices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TimeEntries_Invoices_InvoiceId",
                table: "TimeEntries");

            migrationBuilder.DropIndex(
                name: "IX_TimeEntries_InvoiceId",
                table: "TimeEntries");

            migrationBuilder.DropColumn(
                name: "InvoiceId",
                table: "TimeEntries");

            migrationBuilder.RenameColumn(
                name: "Number",
                table: "Invoices",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "ApprovalStatus",
                table: "InvoiceEntries",
                newName: "approvalStatus");
        }
    }
}
