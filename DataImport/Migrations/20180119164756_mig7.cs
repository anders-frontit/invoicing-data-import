﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FrontIT.DataImport.Migrations
{
    public partial class mig7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "InvoiceId",
                table: "HistoricInvoiceEntries",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_HistoricInvoiceEntries_InvoiceId",
                table: "HistoricInvoiceEntries",
                column: "InvoiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_HistoricInvoiceEntries_Invoices_InvoiceId",
                table: "HistoricInvoiceEntries",
                column: "InvoiceId",
                principalTable: "Invoices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HistoricInvoiceEntries_Invoices_InvoiceId",
                table: "HistoricInvoiceEntries");

            migrationBuilder.DropIndex(
                name: "IX_HistoricInvoiceEntries_InvoiceId",
                table: "HistoricInvoiceEntries");

            migrationBuilder.DropColumn(
                name: "InvoiceId",
                table: "HistoricInvoiceEntries");
        }
    }
}
