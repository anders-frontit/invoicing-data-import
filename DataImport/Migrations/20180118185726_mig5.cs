﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FrontIT.DataImport.Migrations
{
    public partial class mig5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HistoricInvoiceEntries_TimeEntries_TimeEntryId",
                table: "HistoricInvoiceEntries");

            migrationBuilder.RenameColumn(
                name: "TimeEntryId",
                table: "HistoricInvoiceEntries",
                newName: "InvoiceEntryId");

            migrationBuilder.RenameIndex(
                name: "IX_HistoricInvoiceEntries_TimeEntryId",
                table: "HistoricInvoiceEntries",
                newName: "IX_HistoricInvoiceEntries_InvoiceEntryId");

            migrationBuilder.AddForeignKey(
                name: "FK_HistoricInvoiceEntries_InvoiceEntries_InvoiceEntryId",
                table: "HistoricInvoiceEntries",
                column: "InvoiceEntryId",
                principalTable: "InvoiceEntries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HistoricInvoiceEntries_InvoiceEntries_InvoiceEntryId",
                table: "HistoricInvoiceEntries");

            migrationBuilder.RenameColumn(
                name: "InvoiceEntryId",
                table: "HistoricInvoiceEntries",
                newName: "TimeEntryId");

            migrationBuilder.RenameIndex(
                name: "IX_HistoricInvoiceEntries_InvoiceEntryId",
                table: "HistoricInvoiceEntries",
                newName: "IX_HistoricInvoiceEntries_TimeEntryId");

            migrationBuilder.AddForeignKey(
                name: "FK_HistoricInvoiceEntries_TimeEntries_TimeEntryId",
                table: "HistoricInvoiceEntries",
                column: "TimeEntryId",
                principalTable: "TimeEntries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
