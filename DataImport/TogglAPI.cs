﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;

namespace ConsoleApp1
{
    class TogglAPI
    {
        static readonly byte[] token;
        private static readonly string baseAddress;
        //static readonly string reportsAddress;

        static TogglAPI() {
            token = Encoding.ASCII.GetBytes("92db67b015968d78a71b2a5939e29f2c:api_token");
            baseAddress = "https://www.toggl.com";
        }

        public static async Task<List<Workspace>> GetWorkspaces()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(token));
                    var response = await client.GetAsync($"/api/v8/workspaces");
                    response.EnsureSuccessStatusCode();
                    Console.WriteLine("Workspaces retrieved from Toggl");
                    var stringResult = await response.Content.ReadAsStringAsync();
                    List<Workspace> togglWorkspaces = JsonConvert.DeserializeObject<List<Workspace>>(stringResult);
                    return togglWorkspaces;
                }
                catch (HttpRequestException httpRequestException)
                {
                    string result = String.Format("Error getting workspaces from Toggl: {0}", httpRequestException.Message);
                    Console.WriteLine(result);
                }
            }
            return null;
        }

        public static async Task<List<Client>> GetClients(int workspaceId)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(token));
                    var response = await client.GetAsync($"/api/v8/workspaces/{workspaceId}/clients");
                    response.EnsureSuccessStatusCode();
                    var stringResult = await response.Content.ReadAsStringAsync();
                    List<Client> togglClients = JsonConvert.DeserializeObject<List<Client>>(stringResult);
                    return togglClients;
                }
                catch (HttpRequestException httpRequestException)
                {
                    string result = String.Format("Error getting clients from Toggl: {0}", httpRequestException.Message);
                    Console.WriteLine(result);
                }
            }
            return null;
        }

        public static async Task<List<Project>> GetProjects(int workspaceId)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(token));
                    var response = await client.GetAsync($"/api/v8/workspaces/{workspaceId}/projects");
                    response.EnsureSuccessStatusCode();
                    Console.WriteLine("Projects retrieved from Toggl");
                    var stringResult = await response.Content.ReadAsStringAsync();
                    List<Project> togglProjects = JsonConvert.DeserializeObject<List<Project>>(stringResult);
                    return togglProjects;
                }
                catch (HttpRequestException httpRequestException)
                {
                    string result = String.Format("Error getting projects from Toggl: {0}", httpRequestException.Message);
                    Console.WriteLine(result);
                }
            }
            return null;
        }

        public static async Task<List<Task>> GetTasks(int workspaceId)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(token));
                    var response = await client.GetAsync($"/api/v8/workspaces/{workspaceId}/tasks");
                    response.EnsureSuccessStatusCode();
                    Console.WriteLine("Tasks retrieved from Toggl");
                    var stringResult = await response.Content.ReadAsStringAsync();
                    List<Task> togglTasks = JsonConvert.DeserializeObject<List<Task>>(stringResult);
                    return togglTasks;
                }
                catch (HttpRequestException httpRequestException)
                {
                    string result = String.Format("Error getting tasks from Toggl: {0}", httpRequestException.Message);
                    Console.WriteLine(result);
                }
            }
            return null;
        }

        public static async Task<List<Tag>> GetTags(int workspaceId)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(token));
                    var response = await client.GetAsync($"/api/v8/workspaces/{workspaceId}/tags");
                    response.EnsureSuccessStatusCode();
                    Console.WriteLine("Tags retrieved from Toggl");
                    var stringResult = await response.Content.ReadAsStringAsync();
                    List<Tag> togglTags = JsonConvert.DeserializeObject<List<Tag>>(stringResult);
                    return togglTags;
                }
                catch (HttpRequestException httpRequestException)
                {
                    string result = String.Format("Error getting tags from Toggl: {0}", httpRequestException.Message);
                    Console.WriteLine(result);
                }
            }
            return null;
        }

        public static async Task<List<Group>> GetGroups(int workspaceId)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(token));
                    var response = await client.GetAsync($"/api/v8/workspaces/{workspaceId}/groups");
                    response.EnsureSuccessStatusCode();
                    Console.WriteLine("Groups retrieved from Toggl");
                    var stringResult = await response.Content.ReadAsStringAsync();
                    List<Group> togglGroups = JsonConvert.DeserializeObject<List<Group>>(stringResult);
                    return togglGroups;
                }
                catch (HttpRequestException httpRequestException)
                {
                    string result = String.Format("Error getting groups from Toggl: {0}", httpRequestException.Message);
                    Console.WriteLine(result);
                }
            }
            return null;
        }

        public static async Task<List<User>> GetUsers(int workspaceId)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(token));
                    var response = await client.GetAsync($"/api/v8/workspaces/{workspaceId}/users");
                    response.EnsureSuccessStatusCode();
                    Console.WriteLine("Users retrieved from Toggl");
                    var stringResult = await response.Content.ReadAsStringAsync();
                    List<User> togglUsers = JsonConvert.DeserializeObject<List<User>>(stringResult);
                    return togglUsers;
                }
                catch (HttpRequestException httpRequestException)
                {
                    string result = String.Format("Error getting users from Toggl: {0}", httpRequestException.Message);
                    Console.WriteLine(result);
                }
            }
            return null;
        }

        public static async Task<List<TimeEntry>> GetTimeEntries(int workspaceId, DateTimeOffset startAt, DateTimeOffset stopAt)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    //format: /api/v8/time_entries?start_date=2017-10-15T06:42:46+02:00&end_date=2017-12-31T15:42:46+02:00
                    //encoded format: /api/v8/time_entries?start_date=2017-10-15T06%3A42%3A46%2B02%3A00&end_date=2017-12-31T15%3A42%3A46%2B02%3A00
                    string apiPath = "/api/v8/time_entries?start_date=::start::&end_date=::end::";
                    string encodedStart = WebUtility.UrlEncode(startAt.ToString("yyyy-MM-ddTHH:mm:sszzz"));
                    string encodedEnd = WebUtility.UrlEncode(stopAt.ToString("yyyy-MM-ddTHH:mm:sszzz"));
                    string pathWithEncodedDates = apiPath.Replace("::start::", encodedStart).Replace("::end::", encodedEnd);
                    Console.WriteLine(pathWithEncodedDates);
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(token));
                    var response = await client.GetAsync(pathWithEncodedDates);
                    response.EnsureSuccessStatusCode();
                    Console.WriteLine("Time entries retrieved from Toggl");
                    var stringResult = await response.Content.ReadAsStringAsync();
                    List<TimeEntry> togglEntries = JsonConvert.DeserializeObject<List<TimeEntry>>(stringResult);
                    return togglEntries;
                }
                catch (HttpRequestException httpRequestException)
                {
                    string result = String.Format("Error getting time entries from Toggl: {0}", httpRequestException.Message);
                    Console.WriteLine(result);
                }
            }
            return null;
        }

        public static async Task<List<TimeEntry>> GetTimeEntriesByProject(int workspaceId, int projectId, DateTimeOffset startAt, List<TimeEntry> TimeEntriesByProjectAndMonth, int pageNo)
        { 
            using(var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(token));
                    string path = $"/reports/api/v2/details?workspace_id={workspaceId}&project_ids={projectId}&since={startAt.ToString("yyyy-MM-dd")}&until={startAt.AddMonths(1).ToString("yyyy-MM-dd")}&user_agent=as@frontit.dk&order_desc=off&page={pageNo}";
                    var response = await client.GetAsync(path);
                    response.EnsureSuccessStatusCode();
                    //Console.WriteLine($"Time entries on project {projectId} for a month retrieved from Toggl - page {pageNo}");
                    var stringResult = await response.Content.ReadAsStringAsync();
                    DetailedReport report = JsonConvert.DeserializeObject<DetailedReport>(stringResult);
                    int pageCount = (int)Math.Ceiling((double)report.TotalCount / report.PerPage);
                    if(report.TimeEntries != null)
                    {
                        TimeEntriesByProjectAndMonth.AddRange(report.TimeEntries);
                    }
                    //Console.WriteLine("So far we have this many time entries: {0}", TimeEntriesByProjectAndMonth.Count().ToString());
                    if(pageNo < pageCount)
                    {
                        TimeEntriesByProjectAndMonth = await GetTimeEntriesByProject(workspaceId, projectId, startAt, TimeEntriesByProjectAndMonth, (pageNo + 1));
                    } 
                }
                catch (HttpRequestException httpRequestException)
                {
                    string result = String.Format("Error getting time entries from Toggl: {0}", httpRequestException.Message);
                    Console.WriteLine(result);
                }
            }
            return TimeEntriesByProjectAndMonth;
        }
    }
}
