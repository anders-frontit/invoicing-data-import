﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Http.Headers;
using Microsoft.EntityFrameworkCore;

namespace ConsoleApp1
{
    class Import
    {
        //This imports all data - was used during development. During production data should be imported using commands from command line
        static public async void ImportData()
        {
            var spaces = await AssemblaAPI.GetSpaces();
            Data.PersistSpaces(spaces);
            foreach (Space space in spaces)
            {
                if(space.Name.IndexOf("closed") == -1)//Skipping all tickets which have closed in the name - as they are deemed not to be in use
                {
                    var tickets = await AssemblaAPI.GetTickets(space.Id, new List<Ticket>(), 1);
                    if(tickets != null)
                    {
                        Console.WriteLine("Got this amount of tasks for {0}: {1}", space.Name, tickets.Count());
                        Data.PersistTickets(tickets);
                        //Console.WriteLine(tickets.Count());
                    }
                }
            }

            var workspaces = await TogglAPI.GetWorkspaces();
            Data.PersistWorkspaces(workspaces);

            foreach(Workspace ws in workspaces)
            {
                var clients = await TogglAPI.GetClients(ws.Id);
                Data.PersistClients(clients);

                var projects = await TogglAPI.GetProjects(ws.Id);
                Data.PersistProjects(projects);

                var tags = await TogglAPI.GetTags(ws.Id);
                Data.PersistTags(tags);

                var groups = await TogglAPI.GetGroups(ws.Id);
                Data.PersistGroups(groups);

                var users = await TogglAPI.GetUsers(ws.Id);
                Data.PersistUsers(users);

                var tasks = await TogglAPI.GetTasks(ws.Id);
                Data.PersistTasks(tasks);
                

                //int counter = 0;
                foreach (Project proj in projects)
                {
                    //if (counter >= 66)
                    //{
                        List<TimeEntry> allTimeEntries = await TogglAPI.GetTimeEntriesByProject(ws.Id, proj.Id, new DateTimeOffset(2017, 10, 1, 0, 0, 0, TimeSpan.Zero), new List<TimeEntry>(), 1);
                        Console.WriteLine("Received this amount back after going through all pages: {0} for project: {1}", allTimeEntries.Count(), proj.Name);
                        Data.PersistTimeEntries(allTimeEntries);
                    //}
                    //counter++;
                    //if (counter == 70) { return; }
                }

                //DateTime startAt = new DateTime(2017, 10, 1);
                //DateTime stopAt = startAt.AddMonths(2);
                //var timeEntries = await Toggl.GetTimeEntries(ws.Id, startAt, stopAt);

                //foreach(TimeEntry te in timeEntries)
                //{
                //    Console.WriteLine("entry: {0}, {1}", te.Duration, te.Tags == null ? "" : te.Tags.First());
                //}

            }
        }

        static public void ShowHelp()
        {
            Console.WriteLine("Available commands:");
            Console.WriteLine(" - import clients");
            Console.WriteLine(" - import projects");
            Console.WriteLine(" - import tags");
            Console.WriteLine(" - import groups");
            Console.WriteLine(" - import users");
            Console.WriteLine(" - import tasks");
            Console.WriteLine(" - import time (prompts for year and month)");
            Console.WriteLine(" - import all toggl (imports all data from Toggl - except for time entries");
            Console.WriteLine(" - import spaces");
            Console.WriteLine(" - import tickets");
            Console.WriteLine(" - import all assembla (imports all data from Assembla");
            Console.WriteLine(" - show db");
        }

        static public void ShowDBConnection()
        {
            Console.WriteLine("db connection: " + DataContext.dbConnection + ")");
        }

        static async public void GetClients()
        {
            Workspace ws = Data.GetWorkspace();
            var clients = await TogglAPI.GetClients(ws.Id);
            Data.PersistClients(clients);

            Console.WriteLine("Clients retrieved");
        }

        static async public void GetProjects()
        {
            Workspace ws = Data.GetWorkspace();
            var projects = await TogglAPI.GetProjects(ws.Id);
            Data.PersistProjects(projects);
            Console.WriteLine("Projects retrieved");
        }


        static async public void GetTags()
        {
            Workspace ws = Data.GetWorkspace();
            var tags = await TogglAPI.GetTags(ws.Id);
            Data.PersistTags(tags);
            Console.WriteLine("Tags retrieved");
        }

        static async public void GetGroups()
        {
            Workspace ws = Data.GetWorkspace();
            var groups = await TogglAPI.GetGroups(ws.Id);
            Data.PersistGroups(groups);
            Console.WriteLine("Groups retrieved");
        }

        static async public void GetUsers()
        {
            Workspace ws = Data.GetWorkspace();
            var users = await TogglAPI.GetUsers(ws.Id);
            Data.PersistUsers(users);
            Console.WriteLine("Users retrieved");
        }

        static async public void GetTasks()
        {
            Workspace ws = Data.GetWorkspace();
            var tasks = await TogglAPI.GetTasks(ws.Id);
            Data.PersistTasks(tasks);
            Console.WriteLine("Tasks retrieved");
        }

        static async public void GetTimeEntriesByMonth(int year, int month)
        {
            Workspace ws = Data.GetWorkspace();
            IQueryable<Project> projects = Data.GetProjects(ws);
            foreach (Project proj in projects)
            {
                List<TimeEntry> allTimeEntries = await TogglAPI.GetTimeEntriesByProject(ws.Id, proj.Id, new DateTimeOffset(year, month, 1, 0, 0, 0, TimeSpan.Zero), new List<TimeEntry>(), 1);
                Console.WriteLine("Imported {0} time entries for {1}-{2} for {3}", allTimeEntries.Count(), year, month, proj.Name);
                Data.PersistTimeEntries(allTimeEntries);
            }
            Console.WriteLine("Time entries retrieved");
        }

        static async public void ImportAllFromToogl()
        {
            Workspace ws = Data.GetWorkspace();

            var clients = await TogglAPI.GetClients(ws.Id);
            Data.PersistClients(clients);

            var projects = await TogglAPI.GetProjects(ws.Id);
            Data.PersistProjects(projects);

            var tags = await TogglAPI.GetTags(ws.Id);
            Data.PersistTags(tags);

            var groups = await TogglAPI.GetGroups(ws.Id);
            Data.PersistGroups(groups);

            var users = await TogglAPI.GetUsers(ws.Id);
            Data.PersistUsers(users);

            var tasks = await TogglAPI.GetTasks(ws.Id);
            Data.PersistTasks(tasks);
            Console.WriteLine("All imported (except time entries) from Toggl");
        }

        static async public void ImportAllFromAssembla()
        {
            var spaces = await AssemblaAPI.GetSpaces();
            Data.PersistSpaces(spaces);
            foreach (Space space in spaces)
            {
                if (space.Name.IndexOf("closed") == -1)//Skipping all tickets which have closed in the name - as they are deemed not to be in use
                {
                    var tickets = await AssemblaAPI.GetTickets(space.Id, new List<Ticket>(), 1);
                    if (tickets != null)
                    {
                        Console.WriteLine("Got this amount of tasks for {0}: {1}", space.Name, tickets.Count);
                        Data.PersistTickets(tickets);
                    }
                }
            }
            Console.WriteLine("All imported from Assembla");
        }

        static async public void GetSpaces()
        {
            var spaces = await AssemblaAPI.GetSpaces();
            Data.PersistSpaces(spaces);
            Console.WriteLine("Spaces retrieved");
        }

        static async public void GetTickets()
        {
            var spaces = Data.GetSpaces();
            foreach (Space space in spaces)
            {
                if (space.Name.IndexOf("closed") == -1)//Skipping all tickets which have closed in the name - as they are deemed not to be in use
                {
                    var tickets = await AssemblaAPI.GetTickets(space.Id, new List<Ticket>(), 1);
                    if (tickets != null)
                    {
                        Console.WriteLine("Got this amount of tasks for {0}: {1}", space.Name, tickets.Count);
                        Data.PersistTickets(tickets);
                    }
                }
            }
            Console.WriteLine("Tickets retrieved");
        }

        static async public void ShowData()
        {
            var ws = Data.GetWorkspace();
            //Console.WriteLine("Workspace: {0}", ws.Projects.Count());
            /*
            var clients = Data.GetClients(ws);
            foreach (Client cl in ws.Clients)
            {
                Console.WriteLine(" - Client: {0}", cl.Name);
            }
            */

            /*
            var projects = Data.GetProjects(ws);
            foreach (Project pj in projects)
            {
                Console.WriteLine("Project: {0} has client named: {1}", pj.Name, pj.Client == null ? "ingen" : pj.Client.Name);
            }
            */
        }
    }

}
