﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ConsoleApp1
{
    class Data
    {
        public static Workspace GetWorkspace()
        {
            var db = new DataContext();
            var ws = db.Workspaces.Include(w => w.Clients).First<Workspace>();
            return ws;
        }

        public static void PersistWorkspaces(List<Workspace> togglWorkspaces)
        {
            using (var db = new DataContext())
            {
                foreach (Workspace ws in togglWorkspaces)
                {
                    
                    //Looking for a project in existing db that has same it (using the ToogleId) as the id in Toggl
                    //Adding only in case no existing project is found
                    var existingWorkspace = db.Workspaces.SingleOrDefault(ex => ex.Id == ws.Id);
                    if (existingWorkspace == null)
                    {
                        db.Workspaces.Add(ws);
                        Console.WriteLine("Adding new workspace {0}", ws.Name);
                    }
                    else if (ws.LastUpdatedAt > existingWorkspace.LastUpdatedAt)
                    {
                        Console.WriteLine("Updating existing project {0}", ws.Name);
                        existingWorkspace.LastUpdatedAt = ws.LastUpdatedAt;
                        existingWorkspace.Name = ws.Name;
                        existingWorkspace.LogoURL = ws.LogoURL;
                    }
                }
                var count = db.SaveChanges(true);
                Console.WriteLine("{0} records saved to database", count);
            }
        }

        public static IQueryable<Client> GetClients(Workspace ws)
        {
            var db = new DataContext();
            var clients = db.Clients.Include(c => c.Workspace).Where(c => c.WorkspaceId == ws.Id);
            return clients;
        }

        public static void PersistClients(List<Client> togglClients)
        {
            using (var db = new DataContext())
            {
                //Workspace workspace = db.Workspaces.Single(w => w.Id == workspaceId);
                foreach (Client cl in togglClients)
                {
                    //Looking for a project in existing db that has same it (using the ToogleId) as the id in Toggl
                    //Adding only in case no existing project is found
                    var existingClient = db.Clients.SingleOrDefault(ex => ex.Id == cl.Id);
                    if (existingClient == null)
                    {
                        //cl.Workspace = workspace;//It is not the saved workspace reference (but a workspace object from Toggl instead) so adding it here would cause the workspace to become duplicated
                        //cl.WorkspaceId = workspace.Id;
                        db.Clients.Add(cl);
                        Console.WriteLine("Adding new client {0}", cl.Name);
                    }
                    else if (cl.LastUpdatedAt > existingClient.LastUpdatedAt)
                    {
                        Console.WriteLine("Updating existing client {0}", cl.Name);
                        existingClient.LastUpdatedAt = cl.LastUpdatedAt;
                        existingClient.Name = cl.Name;
                        existingClient.Notes = cl.Notes;
                    }

                }
                var count = db.SaveChanges(true);
                Console.WriteLine("{0} records saved to database", count);
            }
        }

        public static IQueryable<Project> GetProjects(Workspace ws)
        {
            var db = new DataContext();
            var projects = db.Projects.Include(p => p.Workspace).Include(p => p.Client).Where(p => p.WorkspaceId == ws.Id);
            return projects;
        }

        public static void PersistProjects(List<Project> togglProjects)
        {
            using (var db = new DataContext())
            {
                foreach (Project pj in togglProjects)
                {
                    //Looking for a project in existing db that has same it (using the ToogleId) as the id in Toggl
                    //Adding only in case no existing project is found
                    var existingProject = db.Projects.SingleOrDefault(ex => ex.Id == pj.Id);
                    if (existingProject == null)
                    {
                        //pj.Workspace = workspace;//It is not the saved workspace reference (but a workspace object from Toggl instead) so adding it here would cause the workspace to become duplicated
                        db.Projects.Add(pj);
                        Console.WriteLine("Adding new project {0}", pj.Name);
                    }
                    else if (pj.LastUpdatedAt > existingProject.LastUpdatedAt)
                    {
                        Console.WriteLine("Updating existing project {0}", pj.Name);
                        existingProject.LastUpdatedAt = pj.LastUpdatedAt;
                        existingProject.Name = pj.Name;
                        existingProject.Active = pj.Active;
                        existingProject.ActualHours = pj.ActualHours;
                        existingProject.AutoEstimates = pj.AutoEstimates;
                        existingProject.Billable = pj.Billable;
                        existingProject.EstimatedHours = pj.EstimatedHours;
                        existingProject.IsPrivate = pj.IsPrivate;
                        existingProject.Template = pj.Template;
                        existingProject.ClientId = pj.ClientId;
                    }

                }
                var count = db.SaveChanges(true);
                Console.WriteLine("{0} records saved to database", count);
            }
        }

        public static void PersistTasks(List<Task> togglTasks)
        {
            using (var db = new DataContext())
            {
                foreach (Task t in togglTasks)
                {
                    var existingTask = db.Tasks.SingleOrDefault(ex => ex.Id == t.Id);
                    if (existingTask == null)
                    {
                        if (t.UserId.HasValue && !db.Users.Any(u => u.Id == t.UserId.Value))
                        {
                            Console.WriteLine("No user existed for task named: {0}", t.Name);
                            t.UserId = null;
                        }

                        if (!db.Projects.Any(p => p.Id == t.ProjectId))
                        {
                            //Console.WriteLine("No project existed for task named: {0}", t.Name);
                            //t.ProjectId = null;
                        } else
                        {
                            db.Tasks.Add(t);
                        }
                        //Console.WriteLine("Adding new task with projectId", t.ProjectId);
                    }
                    else if (t.LastUpdatedAt > existingTask.LastUpdatedAt)
                    {
                        Console.WriteLine("Updating existing task {0}", t.Name);
                        existingTask.LastUpdatedAt = t.LastUpdatedAt;
                        existingTask.Name = t.Name;
                        existingTask.Active = t.Active;
                        existingTask.EstimatedSeconds = t.EstimatedSeconds;
                        existingTask.TrackedSeconds = t.TrackedSeconds;
                        existingTask.UserId = t.UserId;
                    }
                }
                var count = db.SaveChanges(true);
                Console.WriteLine("{0} records saved to database", count);
            }
        }

        public static void PersistTags(List<Tag> togglTags)
        {
            using (var db = new DataContext())
            {
                foreach (Tag t in togglTags)
                {
                    var existingTag = db.Tags.SingleOrDefault(ex => ex.Id == t.Id);
                    if (existingTag == null)
                    {
                        db.Tags.Add(t);
                        Console.WriteLine("Adding new tag: {0}", t.Name);
                    }
                    else if (t.LastUpdatedAt > existingTag.LastUpdatedAt)
                    {
                        Console.WriteLine("Updating existing tag {0}", t.Name);
                        existingTag.LastUpdatedAt = t.LastUpdatedAt;
                        existingTag.Name = t.Name;
                    }
                }
                var count = db.SaveChanges(true);
                Console.WriteLine("{0} records saved to database", count);
            }
        }

        public static void PersistGroups(List<Group> togglGroups)
        {
            using (var db = new DataContext())
            {
                foreach (Group g in togglGroups)
                {
                    var existingGroup = db.Grops.SingleOrDefault(ex => ex.Id == g.Id);
                    if (existingGroup == null)
                    {
                        db.Grops.Add(g);
                        Console.WriteLine("Adding new group: {0}", g.Name);
                    }
                    else if (g.LastUpdatedAt > existingGroup.LastUpdatedAt)
                    {
                        Console.WriteLine("Updating existing group {0}", g.Name);
                        existingGroup.LastUpdatedAt = g.LastUpdatedAt;
                        existingGroup.Name = g.Name;
                    }
                }
                var count = db.SaveChanges(true);
                Console.WriteLine("{0} records saved to database", count);
            }
        }

        public static void PersistUsers(List<User> togglUsers)
        {
            using (var db = new DataContext())
            {
                foreach (User u in togglUsers)
                {
                    var existingUser = db.Users.SingleOrDefault(ex => ex.Id == u.Id);
                    if (existingUser == null)
                    {
                        db.Users.Add(u);
                        Console.WriteLine("Adding new user: {0}", u.Fullname);
                    }
                    else if (u.LastUpdatedAt > existingUser.LastUpdatedAt)
                    {
                        Console.WriteLine("Updating existing user {0}", u.Fullname);
                        existingUser.LastUpdatedAt = u.LastUpdatedAt;
                        existingUser.Fullname = u.Fullname;
                        //existingUser.DefaultWorkspaceId = u.DefaultWorkspaceId;//Not sure about consequences if this is changed
                        existingUser.Email = u.Email;
                        existingUser.ImageURL = u.ImageURL;
                    }
                }
                var count = db.SaveChanges(true);
                Console.WriteLine("{0} records saved to database", count);
            }
        }

        public static IQueryable<TimeEntry> GetTimeEntries(int invoiceId)
        {
            var db = new DataContext();
            var entries = db.TimeEntries.Where(te => te.InvoiceId == invoiceId);
            return entries;
        }


        public static void PersistTimeEntries(List<TimeEntry> togglTimeEntries)
        {
            using (var db = new DataContext())
            {
                foreach (TimeEntry te in togglTimeEntries)
                {
                    /*
                    if(te.ProjectId == 6531872)
                    {
                        Console.WriteLine(te.Description + " :: " + te.TaskName);
                    }
                    if ((!String.IsNullOrEmpty(te.Description) && te.Description.IndexOf("adding") > -1) || (!String.IsNullOrEmpty(te.TaskName) && te.TaskName.IndexOf("adding") > -1))
                    {
                        Console.WriteLine(te.Description + " :: " + te.TaskName);
                    }
                    */
                    var existingTimeEntry = db.TimeEntries.SingleOrDefault(ex => ex.Id == te.Id);
                    
                    if (te.TaskId.HasValue && !db.Tasks.Any(t => t.Id == te.TaskId.Value))
                    {
                        Console.WriteLine("No task existed for time emntry named: {0}", te.TaskName);
                        te.TaskId = null;
                    }

                    if (existingTimeEntry == null)
                    {
                        db.TimeEntries.Add(te);
                        //Console.WriteLine("Adding new time entry: {0}", te.Start.ToLongDateString());
                    }
                    else if (te.LastUpdatedAt > existingTimeEntry.LastUpdatedAt)
                    {
                        //Console.WriteLine("Updating existing time entry {0}", te.Start.ToLongDateString());
                        existingTimeEntry.LastUpdatedAt = te.LastUpdatedAt;
                        existingTimeEntry.Billable = te.Billable;
                        existingTimeEntry.Duration = te.Duration;
                        existingTimeEntry.Start = te.Start;
                        existingTimeEntry.Stop = te.Stop;
                        //existingTimeEntry.Tags = te.Tags;
                        existingTimeEntry.IsBillable = te.IsBillable;
                        existingTimeEntry.Tags = te.Tags;
                        existingTimeEntry.ProjectId = te.ProjectId;
                        existingTimeEntry.TaskId = te.TaskId;
                        existingTimeEntry.Description = te.Description;
                        existingTimeEntry.TaskName = te.TaskName;
                        existingTimeEntry.UserName = te.UserName;
                        existingTimeEntry.ProjectName = te.ProjectName;
                    }
                }
                var count = db.SaveChanges(true);
                Console.WriteLine("{0} records saved to database", count);
            }
        }

        public static void PersistSpaces(List<Space> assemblaSpaces)
        {
            using (var db = new DataContext())
            {
                foreach (Space s in assemblaSpaces)
                {
                    var existingSpace = db.Spaces.SingleOrDefault(ex => ex.Id == s.Id);
                    if (existingSpace == null)
                    {
                        db.Spaces.Add(s);
                        Console.WriteLine("Adding new space: {0}", s.Name);
                    }
                    else if (s.UpdatedAt > existingSpace.UpdatedAt)
                    {
                        Console.WriteLine("Updating existing space {0}", s.Name);
                        existingSpace.UpdatedAt = s.UpdatedAt;
                        existingSpace.Name = s.Name;
                        existingSpace.CreatedAt = s.CreatedAt;
                        existingSpace.Description = s.Description;
                        existingSpace.Status = s.Status;
                        existingSpace.WikiName = s.WikiName;
                    }
                }
                var count = db.SaveChanges(true);
                Console.WriteLine("{0} records saved to database", count);
            }
        }

        public static IQueryable<Space> GetSpaces()
        {
            var db = new DataContext();
            IQueryable<Space> spaces = db.Spaces;
            return spaces;
        }

        public static void PersistTickets(List<Ticket> assemblaTickets)
        {
            using (var db = new DataContext())
            {
                foreach (Ticket t in assemblaTickets)
                {
                    var existingTicket = db.Tickets.SingleOrDefault(ex => ex.Id == t.Id);
                    if (existingTicket == null)
                    {
                        db.Tickets.Add(t);
                        Console.WriteLine("Adding new ticket: {0} - {1}", t.Number, t.Summary);
                    }
                    else if (t.UpdatedAt > existingTicket.UpdatedAt)
                    {
                        Console.WriteLine("Updating existing ticket {0} - {1}", t.Number, t.Summary);
                        existingTicket.UpdatedAt = t.UpdatedAt;
                        existingTicket.Number = t.Number;
                        existingTicket.CompletedDate = t.CompletedDate;
                        existingTicket.CreatedOn = t.CreatedOn;
                        existingTicket.Estimate = t.Estimate;
                        existingTicket.Priority = t.Priority;
                        existingTicket.State = t.State;
                        existingTicket.Status = t.Status;
                        existingTicket.Summary = t.Summary;
                        existingTicket.TotalEstimate = t.TotalEstimate;
                        existingTicket.TotalInvestedHours = t.TotalInvestedHours;
                        existingTicket.TotalWorkingHours = t.TotalWorkingHours;
                        existingTicket.WorkingHours = t.WorkingHours;
                    }
                }
                var count = db.SaveChanges(true);
                Console.WriteLine("{0} records saved to database", count);
            }
        }
    }

    public class DataContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Workspace> Workspaces { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Group> Grops { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<TimeEntry> TimeEntries { get; set; }
        public DbSet<Space> Spaces { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceEntry> InvoiceEntries { get; set; }
        public DbSet<InvoiceEntryHistory> HistoricInvoiceEntries { get; set; }

        public static readonly string dbConnection = ConfigurationManager.ConnectionStrings["mainDatabase"].ConnectionString;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ConsoleApp1.NewDb;Trusted_Connection=True;");
            //optionsBuilder.UseSqlServer(@"Data Source=DESKTOP-FP91V2K\SQLEXPRESS;Initial Catalog=invoice_live;Integrated Security=True;Pooling=False;");
            //optionsBuilder.UseSqlServer(@"Data Source=Lenovo-PC\SQLEXPRESS;Initial Catalog=invoice_test;Integrated Security=True;Pooling=False;");
            //Reading from connection string in app.config used to work well but suddenly it stopped working for when using update-migration from package manager console
            optionsBuilder.UseSqlServer(dbConnection); //For the "script-migration" command this method reported an error. So if needing to run that command use the method above with a hardcoded string
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /*
            modelBuilder.Entity<Workspace>()
                .HasIndex(b => b.TogglId)
                .IsUnique();

            modelBuilder.Entity<Client>()
                .HasIndex(b => b.TogglId)
                .IsUnique();

            modelBuilder.Entity<Project>()
                .HasIndex(b => b.TogglId)
                .IsUnique();
            */
            modelBuilder.Entity<Project>().HasOne(p => p.Workspace).WithMany(w => w.Projects).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Task>().HasOne(t => t.Workspace).WithMany(w => w.Tasks).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Client>().HasOne(c => c.Workspace).WithMany(w => w.Clients).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<User>().HasOne(c => c.Workspace).WithMany(w => w.Users).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<InvoiceEntryHistory>().HasOne(h => h.Invoice).WithMany(i => i.HistoricalInvoiceEntries).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Invoice>().HasOne(i => i.Client).WithMany(c => c.Invoices).OnDelete(DeleteBehavior.Restrict);
            //modelBuilder.Entity<InvoiceEntry>().HasOne(i => i.Invoice).WithMany(w => w.InvoiceEntries).OnDelete(DeleteBehavior.Restrict);
            //modelBuilder.Entity<TimeEntry>().HasOne(i => i.Invoice).WithMany(w => w.TimeEntries).OnDelete(DeleteBehavior.Restrict);
            //modelBuilder.Entity<TimeEntry>().HasOne(te => te.Workspace).WithMany(w => w.TimeEntries).OnDelete(DeleteBehavior.Restrict);
            //modelBuilder.Entity<TimeEntry>().HasOne(te => te.Task).WithMany(t => t.TimeEntries).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
