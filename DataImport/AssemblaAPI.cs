﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;

namespace ConsoleApp1
{
    public class AssemblaAPI
    {
        private static readonly string apiKey;
        private static readonly string apiSecret;
        private static readonly string baseAddress;
        private static readonly int entriesPerRequest = 100;

        static AssemblaAPI()
        {
            apiKey = "e150bb39e8c0ef8fe1d3";
            apiSecret = "3a9a86fa8ab3a0b6dce6d5f108e6b104152f7c1c";
            baseAddress = "https://api.assembla.com/";
        }

        public static async Task<List<Space>> GetSpaces()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Add("X-Api-Key", apiKey);
                    client.DefaultRequestHeaders.Add("X-Api-Secret", apiSecret);
                    var response = await client.GetAsync("/v1/spaces.json");
                    response.EnsureSuccessStatusCode();
                    Console.WriteLine("Spaces retrieved from Assembla");
                    var stringResult = await response.Content.ReadAsStringAsync();
                    List<Space> assemblaSpaces = JsonConvert.DeserializeObject<List<Space>>(stringResult);
                    return assemblaSpaces;
                }
                catch (HttpRequestException httpRequestException)
                {
                    string result = String.Format("Error getting spaces from Assembla: {0}", httpRequestException.Message);
                    Console.WriteLine(result);
                }
            }
            return null;
        }

        public static async Task<List<Ticket>> GetTickets(string spaceId, List<Ticket> TaskEntriesBySpace, int pageNo)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Add("X-Api-Key", apiKey);
                    client.DefaultRequestHeaders.Add("X-Api-Secret", apiSecret);
                    //Param "report = 1" only takes tickes which are active - need to agree with team not to make tickets inactive before billing has been done
                    var response = await client.GetAsync($"/v1/spaces/{spaceId}/tickets.json?report=1&per_page={entriesPerRequest}&page={pageNo}");
                    response.EnsureSuccessStatusCode();
                    //Console.WriteLine("Tickets retrieved from Assembla");
                    var stringResult = await response.Content.ReadAsStringAsync();
                    List<Ticket> assemblaTickets = JsonConvert.DeserializeObject<List<Ticket>>(stringResult);
                    if (assemblaTickets != null)
                    {
                        TaskEntriesBySpace.AddRange(assemblaTickets);
                        //Console.WriteLine("So far we have this many tasks entries: {0}", TaskEntriesBySpace.Count().ToString());
                        if (assemblaTickets.Count() == entriesPerRequest)//We got a full page/load of entries back and so there might be more
                        {
                            //Console.WriteLine("Going for more task entries");
                            TaskEntriesBySpace = await GetTickets(spaceId, TaskEntriesBySpace, (pageNo + 1));
                        }
                    }
                }
                catch (HttpRequestException httpRequestException)
                {
                    string result = String.Format("Error getting tickets from Assembla: {0}", httpRequestException.Message);
                    Console.WriteLine(result);
                }
            }
            return TaskEntriesBySpace;
        }

    }
}
