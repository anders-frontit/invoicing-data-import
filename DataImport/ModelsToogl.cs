﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConsoleApp1
{
    public class Project
    {
        public Project()
        {
            //Tasks = new List<Task>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Billable { get; set; }
        [JsonProperty("is_private")]
        public bool IsPrivate { get; set; }
        public bool Active { get; set; }
        public bool Template { get; set; }
        [JsonProperty("auto_estimates")]
        public bool AutoEstimates { get; set; }
        [JsonProperty("estimated_hours")]
        public int EstimatedHours { get; set; }
        [JsonProperty("actual_hours")]
        public int ActualHours { get; set; }
        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }
        [JsonProperty("at")]
        public DateTimeOffset LastUpdatedAt { get; set; }

        [JsonProperty("cid")]
        public int? ClientId { get; set; }
        public Client Client { get; set; }

        [JsonProperty("wid")]
        public int WorkspaceId { get; set; }
        public Workspace Workspace { get; set; }

        public List<Task> Tasks { get; set; }
        public List<TimeEntry> TimeEntries { get; set; }
    }

    public class Task
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        [JsonProperty("estimated_seconds")]
        public int EstimatedSeconds { get; set; }
        [JsonProperty("tracked_seconds")]
        public int TrackedSeconds { get; set; }
        [JsonProperty("at")]
        public DateTimeOffset LastUpdatedAt { get; set; }

        [JsonProperty("pid")]
        public int ProjectId { get; set; }
        public Project Project { get; set; }

        [JsonProperty("wid")]
        public int WorkspaceId { get; set; }
        public Workspace Workspace { get; set; }

        [JsonProperty("uid")]
        public int? UserId { get; set; }
        public User User { get; set; }

        public List<TimeEntry> TimeEntries { get; set; }
    }

    public class Client
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonProperty("at")]
        public DateTimeOffset LastUpdatedAt { get; set; }
        public string Notes { get; set; }
        public string InvoiceSummary { get; set; }
        public string InvoiceRecipient { get; set; }
        public bool SummarizeInvoice { get; set; }

        [JsonProperty("wid")]
        public int WorkspaceId { get; set; }
        public Workspace Workspace { get; set; }

        public List<Invoice> Invoices { get; set; }
        public List<Project> Projects { get; set; }
    }

    public class Workspace
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonProperty("at")]
        public DateTimeOffset LastUpdatedAt { get; set; }
        [JsonProperty("logo_url")]
        public string LogoURL { get; set; }

        public List<Client> Clients { get; set; }
        public List<Task> Tasks { get; set; }
        public List<Project> Projects { get; set; }
        public List<Tag> Tags { get; set; }
        public List<Group> Groups { get; set; }
        public List<User> Users { get; set; }
        public List<TimeEntry> TimeEntries { get; set; }
    }

    public class Tag
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonProperty("at")]
        public DateTimeOffset LastUpdatedAt { get; set; }

        [JsonProperty("wid")]
        public int WorkspaceId { get; set; }
        public Workspace Workspace { get; set; }
    }

    public enum Tags { Billed }

    public class Group
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonProperty("at")]
        public DateTimeOffset LastUpdatedAt { get; set; }

        [JsonProperty("wid")]
        public int WorkspaceId { get; set; }
        public Workspace Workspace { get; set; }
    }

    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Email { get; set; }
        public string Fullname { get; set; }
        [JsonProperty("image_url")]
        public string ImageURL { get; set; }
        [JsonProperty("at")]
        public DateTimeOffset LastUpdatedAt { get; set; }

        [JsonProperty("wid")]
        public int DefaultWorkspaceId { get; set; }
        public Workspace Workspace { get; set; }

        public List<Task> Tasks { get; set; }
        public List<TimeEntry> TimeEntries { get; set; }
    }

    public class TimeEntry
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        //"guid": "ce56c8b38d5191134feeebe09a4a6795",
        public double Billable { get; set; }
        [JsonProperty("is_billable")]
        public bool IsBillable { get; set; }
        public DateTimeOffset Start { get; set; }
        [JsonProperty("end")]
        public DateTimeOffset? Stop { get; set; }
        public string Description { get; set; }//Might be empty or having a unique value - oftentime, though, it is the same as the task name

        [JsonProperty("dur")]
        public int Duration { get; set; }

        //[JsonProperty("duronly")]
        //public bool DurOnly { get; set; }

        [JsonProperty("updated")]//For time entry in the report this is "updated" otherwise it is "at"
        public DateTimeOffset LastUpdatedAt { get; set; }

        [JsonProperty("tid")]
        public int? TaskId { get; set; }

        [JsonProperty("task")]
        public string TaskName { get; set; }

        [JsonProperty("pid")]
        public int ProjectId { get; set; }

        [JsonProperty("project")]
        public string ProjectName { get; set; }

        [JsonProperty("client")]
        public string ClientName { get; set; }

        [JsonProperty("uid")]
        public int UserId { get; set; }

        [JsonProperty("user")]
        public string UserName { get; set; }

        public int? InvoiceId { get; set; }
        public Invoice Invoice { get; set; }

        //[JsonProperty("wid")]
        //public int WorkspaceId { get; set; }
        //public Workspace Workspace { get; set; }

        //private ICollection<string> _strings { get; set; }
        // EF-acceptable reference to an 'array' of doubles
        //public virtual List<Primitive> Tags { get; set; }

        [NotMapped]
        [JsonProperty("tags")]
        public IEnumerable<string> InternalTags
        {
            get
            {
                if (!string.IsNullOrEmpty(Tags))
                {
                    var tab = Tags.Split(',');
                    return tab;//.Select(double.Parse).AsEnumerable();
                }
                return null;
            }
            set {
                Tags = string.Join(",", value);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [JsonIgnore()]
        public string Tags { get; set; }
    }

    public class DetailedReport
    {
        [JsonProperty("total_grand")]
        public long? TotalGrand { get; set; }
        [JsonProperty("total_billable")]
        public long? TotalBillable { get; set; }
        [JsonProperty("total_count")]
        public int TotalCount { get; set; }
        [JsonProperty("per_page")]
        public int PerPage { get; set; }

        [JsonProperty("data")]
        public List<TimeEntry> TimeEntries { get; set; }
    }

}
