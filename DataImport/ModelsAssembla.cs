﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;


namespace ConsoleApp1
{
    public class Space
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [JsonProperty("wiki_name")]
        public string WikiName { get; set; }
        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }
        [JsonProperty("updated_at")]
        public DateTimeOffset UpdatedAt { get; set; }
        public short Status { get; set; }

        public List<Ticket> Tickets { get; set; }
    }

    public class Ticket
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }
        public int Number { get; set; }
        public string Summary { get; set; }
        public short Priority { get; set; }
        [JsonProperty("completed_date")]
        public DateTimeOffset? CompletedDate { get; set; }
        [JsonProperty("created_on")]
        public DateTimeOffset CreatedOn { get; set; }
        [JsonProperty("updated_at")]
        public DateTimeOffset UpdatedAt { get; set; }
        public short State { get; set; }
        public string Status { get; set; }
        [JsonProperty("working_hours")]
        public double WorkingHours { get; set; }
        public double Estimate { get; set; }
        [JsonProperty("total_estimate")]
        public double TotalEstimate { get; set; }
        [JsonProperty("total_invested_hours")]
        public double TotalInvestedHours { get; set; }
        [JsonProperty("total_working_hours")]
        public double TotalWorkingHours { get; set; }

        [JsonProperty("space_id")]
        public string SpaceId { get; set; }
        public Space Space { get; set; }
    }
}
