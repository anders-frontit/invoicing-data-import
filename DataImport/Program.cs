﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string line;
            Console.WriteLine("Enter a command. Type 'help' to see a list of available commands (press CTRL+Z to exit):");
            Console.WriteLine();
            do
            {
                //Console.Write("   ");
                line = Console.ReadLine();
                if (line != null)
                {
                    //Console.WriteLine("      " + line);
                    switch (line)
                    {
                        case "help":
                            Import.ShowHelp();
                            break;
                        case "show db":
                            Import.ShowDBConnection();
                            break;
                        case "import clients":
                            Import.GetClients();
                            break;
                        case "import projects":
                            Import.GetProjects();
                            break;
                        case "import tags":
                            Import.GetTags();
                            break;
                        case "import groups":
                            Import.GetGroups();
                            break;
                        case "import users":
                            Import.GetUsers();
                            break;
                        case "import tasks":
                            Import.GetTasks();
                            break;
                        case "import all toggl":
                            Import.ImportAllFromToogl();
                            break;
                        case "import spaces":
                            Import.GetSpaces();
                            break;
                        case "import tickets":
                            Import.GetTickets();
                            break;
                        case "import all assembla":
                            Import.ImportAllFromAssembla();
                            break;
                        default:
                            if(line.StartsWith("import time"))
                            {
                                DateTime now = DateTime.Today;
                                Console.WriteLine("Which month and year [{0} {1}]", now.Year.ToString(), now.ToString("MMM"));
                                string time = Console.ReadLine();
                                int year;
                                int month;
                                try
                                {
                                    year = int.Parse(time.Substring(0, 4));
                                    month = DateTime.ParseExact(time.Substring(5, 3), "MMM", System.Globalization.CultureInfo.GetCultureInfo("en-us")).Month;
                                    Import.GetTimeEntriesByMonth(year, month);
                                } catch
                                {
                                    Console.WriteLine("Year and month name not in correct format.");
                                }
                            } else
                            {
                                Console.WriteLine("Unrecognized command");
                            }
                            break;
                    }
                }
            } while (line != null);


            //Import.ImportData();
            //Import.ShowData();

            //Console.WriteLine("Press any key to continue...");
            //Console.ReadKey(true);
        }
    }
}
