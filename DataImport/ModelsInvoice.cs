﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ConsoleApp1
{
    public class Invoice
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Number { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset InvoiceDate { get; set; }
        public bool IsInvoiced { get; set; }

        public int ClientId { get; set; }
        public Client Client { get; set; }
        
        public List<InvoiceEntry> InvoiceEntries { get; set; }
        public List<TimeEntry> TimeEntries { get; set; }
        public List<InvoiceEntryHistory> HistoricalInvoiceEntries { get; set; }
    }

    public class InvoiceEntry
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public int CalculatedTime { get; set; }
        public int SuggestedTime { get; set; }
        public int ApprovedTime { get; set; }
        public string TaskName { get; set; }
        public string EstimatedTime { get; set; }
        public InvoiceEntryStatus ApprovalStatus { get; set; }
        public string Comment { get; set; }
        public bool ShowWhenZero { get; set; }

        public int? TaskId { get; set; }
        public Task Task { get; set; }

        public int? ProjectId { get; set; }
        public Project Project { get; set; }

        public int InvoiceId { get; set; }
        public Invoice Invoice { get; set; }
    }

    public enum InvoiceEntryStatus
    {
        NonApproved,
        Approved
    }
}
